import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:student_chat_app/core/core_components/state_component.dart';
import 'package:student_chat_app/modules/courses/presentation/components/course_view.dart';
import '../controller/all_courses/all_courses_controller.dart';
import '../controller/available_courses/available_courses_binding.dart';
import '../controller/available_courses/available_courses_controller.dart';


class AvailableCoursesScreen extends GetView<AvailableCoursesController> {
  const AvailableCoursesScreen({Key? key}) : super(key: key);

  static const name = '/available_courses_screen';
  static final GetPage page = GetPage(
      name: name,
      page: () => const AvailableCoursesScreen(),
      binding: AvailableCoursesBinding()
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Available Courses'),
      ),
      body: GetBuilder<AvailableCoursesController>(
        builder: (_)=> StatusComponent(
            state: controller.state,
            onSuccess: (_,state) =>
                ListView.separated(
                  padding: EdgeInsets.all(2.5.w),
                  itemCount: controller.data.length,
                  separatorBuilder: (_,i) => const Divider(),
                  itemBuilder: (_,i) =>
                      CourseView(
                        controller.data[i],
                        navigate: true,
                      ),
                )
        ),
      ),
    );
  }
}
