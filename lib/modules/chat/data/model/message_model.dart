
class MessageModel {

  final int id;
  final String question;
  final String answer;

  MessageModel({
    required this.id,
    required this.question,
    required this.answer
  });

  factory MessageModel.formJson(Map<String,dynamic> json)=>
      MessageModel(
        id: json['id'] ,
        answer: json['answer'] ,
        question: json['question']
      );
}