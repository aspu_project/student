import 'package:circle_nav_bar/circle_nav_bar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:student_chat_app/core/consts/color_const.dart';
import 'package:student_chat_app/modules/courses/presentation/screens/all_courses_screen.dart';
import 'package:student_chat_app/modules/courses/presentation/screens/course_screen.dart';
import 'package:student_chat_app/modules/information/presentation/screens/info_screen.dart';

import '../../modules/profile/presentation/presentation/profile_screen.dart';

class BottomNavBar extends StatefulWidget {
  const BottomNavBar({Key? key, required this.inx,}) : super(key: key);
  final int inx;

  @override
  State<BottomNavBar> createState() => _BottomNavBarState();
}

class _BottomNavBarState extends State<BottomNavBar> {

  late int inx;

  @override
  void initState() {
    super.initState();
    inx = widget.inx;
  }

  static const _pages = [
    ProfileScreen.name,
    AllCoursesScreen.name,
    InfoScreen.name
  ];

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: 'nav',

      child: CircleNavBar(
          activeIndex:inx,
          onTap: (newInx){
            if(widget.inx == newInx) return;
            setState(() {
              inx = newInx;
            });
            Future.delayed(const Duration(milliseconds: 150),()=>Get.offNamed(_pages[inx]));
          },
          activeIcons: const [
            Icon(Icons.person_rounded),
            Icon(Icons.school),
            Icon(Icons.subject),
          ],
          inactiveIcons: const [
            Icon(Icons.person_outline_rounded),
            Icon(Icons.school_outlined),
            Icon(Icons.subject_outlined),
          ],
          height: 15.w,
          circleWidth: 15.w,
          color: Colors.grey[900]!,
          shadowColor: Colors.black54,
      ),
    );
  }
}
