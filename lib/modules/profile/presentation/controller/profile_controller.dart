
import 'package:student_chat_app/core/base_controllers/load_data_controller.dart';
import 'package:student_chat_app/modules/profile/data/data_source/profile_data_source.dart';
import 'package:student_chat_app/modules/profile/data/model/user_model.dart';

class ProfileController extends LoadDataController<UserModel>{
  @override
  Future<UserModel> callApi() =>
      ProfileDataSource.getProfile();



}