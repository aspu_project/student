import 'package:flutter/material.dart';

class AppColors {


  static const white = Colors.white;
  static const red = Colors.red;
  static const blue = Color(0xFF525CEB);
  static const lightBlue = Color(0xFFBFCFE7);
  static const darkColor = Color(0xFF3D3B40);
  static const grey = Colors.grey;
  static const Color lightGrey = Color(0xFFF8EDFF);


}
