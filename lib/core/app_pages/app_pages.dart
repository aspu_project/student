import 'package:get/get.dart';

import '../../modules/auth/presentation/screens/login_screen.dart';
import '../../modules/auth/presentation/screens/splash_screen.dart';
import '../../modules/chat/presentation/screens/chat_screen.dart';
import '../../modules/chat/presentation/screens/custom_chat_bot_screen.dart';
import '../../modules/chat/presentation/screens/report_screen.dart';
import '../../modules/courses/presentation/screens/all_courses_screen.dart';
import '../../modules/courses/presentation/screens/available_courses_screen.dart';
import '../../modules/courses/presentation/screens/course_screen.dart';
import '../../modules/information/presentation/screens/info_screen.dart';
import '../../modules/profile/presentation/presentation/profile_screen.dart';

class AppPages {
  static List<GetPage> get pages => [
    SplashScreen.page,
    LoginScreen.page,
    AllCoursesScreen.page,
    InfoScreen.page,
    ChatScreen.page,
    CourseScreen.page,
    ProfileScreen.page,
    ReportScreen.page,
    AvailableCoursesScreen.page,
    CustomChatScreen.page
  ];
}
