import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:student_chat_app/modules/auth/presentation/controllers/splash/splash_binding.dart';
import '../../../../core/consts/color_const.dart';
import '../controllers/splash/splash_controller.dart';

class SplashScreen extends GetView<SplashController> {
  const SplashScreen({Key? key}) : super(key: key);

  static const name = '/';
  static final GetPage page = GetPage(
    name: name,
    page: () => const SplashScreen(),
    binding: SplashBinding()
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: Hero(
              tag: 'splash',
              child: Container(
                color: AppColors.blue,
              )
          )
      ),
    );
  }
}
