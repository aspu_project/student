import 'package:get/get.dart';
import 'package:student_chat_app/modules/auth/presentation/controllers/splash/splash_controller.dart';

class SplashBinding extends Bindings {


  @override
  void dependencies() {
    Get.put(SplashController());
  }
}