import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:student_chat_app/core/core_components/pop_up.dart';
import 'package:student_chat_app/core/handler/handler.dart';
import 'package:student_chat_app/modules/auth/data/data_source/auth_data_source.dart';
import 'package:student_chat_app/modules/courses/presentation/screens/all_courses_screen.dart';

class LoginController extends GetxController {

  final formKey = GlobalKey<FormState>();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passController = TextEditingController();

  void login() async {
    if(!(formKey.currentState?.validate() ?? true)){
      showSnackBar('error , invalid input');
    }
    showLoader();
    var state = await handle(() => AuthDataSource.login(emailController.text, passController.text));
    Get.back();
    state.fold(
            (failState) => showSnackBar(failState.message),
            (successState) => Get.offNamed(AllCoursesScreen.name)
    );
  }
}
