import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AppAnimatedText extends StatefulWidget {
  final String text;
  final TextStyle? style;
  final Duration duration;

  AppAnimatedText(this.text,{Key? key,this.style, required this.duration}) : super(key: key);

  @override
  State<AppAnimatedText> createState() => _AppAnimatedTextState();
}

class _AppAnimatedTextState extends State<AppAnimatedText> {
  String text= '';

  Stream<String> animate() async* {
    for(int i = 0 ; i < widget.text.length ; i ++){
      await Future.delayed(widget.duration);
      yield text ='${widget.text.substring(0,i)}_';
    }
    await Future.delayed(widget.duration);
    text = widget.text;
    yield widget.text;
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<String>(
      stream: animate(),
      builder: (context,snapShot) {
        return Text(text ?? '',style: widget.style,);
      }
    );
  }
}
