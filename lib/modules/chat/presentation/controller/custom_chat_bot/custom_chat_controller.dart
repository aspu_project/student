
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:student_chat_app/core/base_controllers/load_data_controller.dart';
import 'package:student_chat_app/core/core_components/app_loader.dart';
import 'package:student_chat_app/core/core_components/pop_up.dart';
import 'package:student_chat_app/core/handler/handler.dart';
import 'package:student_chat_app/modules/chat/data/data_source/chat_data_source.dart';
import 'package:student_chat_app/modules/chat/data/model/message_model.dart';

import '../../../../../core/data_state/data_state.dart';


class CustomChatController extends LoadDataController<List<MessageModel>> {


  final messageController = TextEditingController();

  @override
  Future<List<MessageModel>> callApi() =>
      ChatDataSource.getOldInfoMessages();


  void send() async{
    if(messageController.text.replaceAll(" ", "").isEmpty){
      return;
    }
    showLoader(
      const MessageLoader()
    );
    var dataState = await handle<MessageModel>(()=>ChatDataSource.sendCustomMessages(messageController.text));
    Get.back();
    dataState.fold(
            (failState) => showSnackBar(failState.message),
            (successState) {
              if(state.status == DataStatus.error){
                loadData();
              }else {
                state.data?.insert(0,successState.data!);
              }
              messageController.text = '';
            }
    );
    update();
  }



}