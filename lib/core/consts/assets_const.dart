
class LottieConst {
  static const _lottiePath = "assets/lottie";
  static const loader = '$_lottiePath/loader.json';
  static const messageLoader = '$_lottiePath/message_loader.json';
}