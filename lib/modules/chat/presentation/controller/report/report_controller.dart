
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:student_chat_app/core/handler/handler.dart';
import 'package:student_chat_app/modules/chat/data/data_source/chat_data_source.dart';
import 'package:student_chat_app/modules/chat/data/model/message_model.dart';

import '../../../../../core/core_components/pop_up.dart';

class ReportController extends GetxController {

  final MessageModel model;
  ReportController(this.model);

  final reasonController = TextEditingController();

  void submit()async{
    if(reasonController.text.isEmpty){
      showSnackBar("Enter a reason!");
    }
    showLoader();
    var state =await handle(() => ChatDataSource.sendReport(reasonController.text, model.id));
    Get.back();
    state.fold(
            (failState) => showSnackBar(failState.message),
            (successState){
              Get.back();
              showSnackBar('reported successfully');
            }
    );
  }
}