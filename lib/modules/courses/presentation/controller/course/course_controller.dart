import 'package:get/get.dart';
import 'package:student_chat_app/core/base_controllers/load_data_controller.dart';
import 'package:student_chat_app/modules/courses/data/data_source/courses_data_source.dart';
import 'package:student_chat_app/modules/courses/data/model/course_model.dart';

class CourseController extends LoadDataController<CourseModel>{

  final CourseModel model;


  CourseController(this.model);

  @override
  Future<CourseModel> callApi() =>
      CoursesDataSource.getById(model.id);

}