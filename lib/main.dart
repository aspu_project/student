import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:student_chat_app/core/app_pages/app_pages.dart';
import 'package:student_chat_app/core/storage/storage_handler.dart';

import 'core/consts/color_const.dart';

void main() async{
  await StorageHandler.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ResponsiveSizer(
        builder: (_,__,___) =>
            GetMaterialApp(
              debugShowCheckedModeBanner: false,
              theme: ThemeData(
                  colorScheme: const ColorScheme.dark(
                      primary: AppColors.darkColor
                  ),
                  appBarTheme: const AppBarTheme(
                      backgroundColor: AppColors.darkColor,
                      centerTitle: true
                  ),
                  cardTheme: CardTheme(
                    color: AppColors.darkColor.withOpacity(0.2)
                  )
              ),
              getPages: AppPages.pages.map(
                      (e) => e.copy(
                        transitionDuration: const Duration(
                          milliseconds: 400,
                        ),
                        transition: Transition.fadeIn,
                      )
              ).toList(),
            )
    );
  }
}
