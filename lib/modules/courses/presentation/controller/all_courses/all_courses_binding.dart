
import 'package:get/get.dart';

import 'all_courses_controller.dart';

class AllCoursesBinding extends Bindings {


  @override
  void dependencies() {
    Get.put(AllCoursesController());
  }
}