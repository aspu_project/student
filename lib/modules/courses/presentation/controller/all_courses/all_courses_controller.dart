import 'package:get/get.dart';
import 'package:student_chat_app/core/base_controllers/load_data_controller.dart';
import 'package:student_chat_app/modules/courses/data/data_source/courses_data_source.dart';
import 'package:student_chat_app/modules/courses/data/model/course_model.dart';

class AllCoursesController extends LoadDataController<List<CourseModel>>{
  @override
  Future<List<CourseModel>> callApi() =>
      CoursesDataSource.getAll();

}