import 'package:flutter/material.dart';

import '../../data/model/exam_model.dart';

class ExamView extends StatelessWidget {
  final ExamModel model;

  const ExamView(this.model,{Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        leading: Icon(
            Icons.edit_outlined,
            color: model.isSuccess ? Colors.green : Colors.red,
        ),
        title: Text(model.course),
        subtitle: Text.rich(
            TextSpan(
                text:'result : ${model.mark} , ',
                children: [
                  TextSpan(
                    text: model.isSuccess ? 'Success':'Fail',
                    style: TextStyle(
                      color: model.isSuccess ? Colors.green : Colors.red,
                    )
                  )
                ]
            )
        ),
      ),
    );
  }
}
