import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:student_chat_app/modules/courses/presentation/screens/all_courses_screen.dart';
import '../../../../core/core_components/bottom_nav_bar.dart';
import '../../../../core/core_components/home_drawer.dart';
import '../../../../core/core_components/state_component.dart';
import '../controller/information_binding.dart';
import '../controller/information_controller.dart';


class InfoScreen extends GetView<InformationController> {
  const InfoScreen({Key? key}) : super(key: key);

  static const name = '/info_screen';
  static final GetPage page = GetPage(
    name: name,
    page: () => const InfoScreen(),
    binding: InformationBindings(),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('public information'),
      ),
      drawer: const HomeDrawer(),
      bottomNavigationBar: const BottomNavBar(inx: 2,),
      body: GetBuilder<InformationController>(
        builder: (_) =>
            StatusComponent(
                state: controller.state,
                onSuccess: (_,state) =>ListView.builder(
                    itemCount: controller.data.length,
                    itemBuilder: (_,i) =>
                        ListTile(
                          title: Text(controller.data[i].title),
                          subtitle: Text(controller.data[i].description),
                        )
                )
            ),
      ),
    );
  }
}
