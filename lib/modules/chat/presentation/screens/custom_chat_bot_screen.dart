import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:student_chat_app/core/core_components/app_loader.dart';
import 'package:student_chat_app/core/core_components/app_text_form_feild.dart';
import 'package:student_chat_app/core/core_components/state_component.dart';
import '../../data/model/message_model.dart';
import '../components/message_component.dart';
import '../controller/custom_chat_bot/custom_chat_binding.dart';
import '../controller/custom_chat_bot/custom_chat_controller.dart';

class CustomChatScreen extends GetView<CustomChatController> {
  const CustomChatScreen({Key? key}) : super(key: key);

  static const name = '/custom_chat';
  static final page = GetPage(
      name: name,
      page: ()=> const CustomChatScreen(),
      binding: CustomChatBinding()
  );


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Custom Chat Bot'),
        ),
        body: GetBuilder<CustomChatController>(
          builder: (_)=>StatusComponent<List<MessageModel>>(
            state: controller.state,
            onSuccess: (_,state) =>
                Column(
                  children: [
                    Expanded(
                        flex: 9,
                        child: ListView.builder(
                            reverse: true,
                            itemCount: state.data!.length,
                            itemBuilder: (context,i) =>
                                MessageComponent(model: state.data![i],animate: i == 0,)
                        )
                    ),
                    Padding(
                      padding: EdgeInsets.all(2.5.w),
                      child: Row(
                        children: [
                          Expanded(
                              child: AppTextFormField(
                                hint: 'Write a question',
                                controller: controller.messageController,
                              )
                          ),
                          IconButton(
                              onPressed: controller.send,
                              icon: Icon(Icons.send)
                          )
                        ],
                      ),
                    ),
                  ],
                ),
            onLoading: (_,__) => const Center(child: MessageLoader()),
          ),
        )
    );
  }
}
