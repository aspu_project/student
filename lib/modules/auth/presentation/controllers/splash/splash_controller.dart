import 'package:get/get.dart';

import '../../screens/login_screen.dart';

class SplashController extends GetxController{

  @override
  void onReady() {
    super.onReady();
    Future.delayed(const Duration(seconds: 2))
        .then((value) => Get.offNamed(LoginScreen.name));
  }

}