
import 'package:get/get.dart';
import 'package:student_chat_app/modules/profile/presentation/controller/profile_controller.dart';

class ProfileBinding extends Bindings {


  @override
  void dependencies() {
    Get.put(ProfileController());
  }
}