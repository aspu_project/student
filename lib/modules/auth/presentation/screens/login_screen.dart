import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:student_chat_app/core/consts/color_const.dart';
import 'package:student_chat_app/core/core_components/app_text_form_feild.dart';
import 'package:student_chat_app/core/core_components/submit_button.dart';
import '../components/login_clipper.dart';
import '../controllers/login/login_binding.dart';
import '../controllers/login/login_controller.dart';
import '../controllers/login/login_middleware.dart';

class LoginScreen extends GetView<LoginController> {
  const LoginScreen({super.key});

  static const name = '/login';
  static final GetPage page = GetPage(
    name: name,
    page: () => const LoginScreen(),
    binding: LoginBindings(),
    middlewares: [
      LoginMiddleware()
    ]
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Hero(
            tag: 'splash',
            child: ClipPath(
              clipper: LoginClipper(),
              child: Container(
                width: double.infinity,
                height: 80.w,
                color: AppColors.blue,
              ),
            ),
          ),
          SafeArea(
              child: SingleChildScrollView(
                padding: EdgeInsets.all(2.5.w),
                child: SizedBox(
                  width: double.infinity,
                  child: Form(
                    key: controller.formKey,
                    child: Column(
                      children: [
                        Text(
                            "Welcome In ASPU Assistance App",
                            style: TextStyle(
                              fontSize: 16.spa,
                              fontWeight: FontWeight.bold,
                            ),
                            textAlign: TextAlign.center,
                        ),
                        SizedBox(
                          height: 12.5.w,
                        ),
                        CircleAvatar(
                          radius: 30.w,
                          foregroundColor: AppColors.lightGrey,
                          child: Icon(
                              Icons.person_rounded,
                              size: 50.w,
                          ),
                        ),
                        Divider(
                          height: 7.5.w,
                          color: AppColors.lightGrey,
                        ),
                        SizedBox(
                          height: 5.w,
                        ),
                        AppTextFormField(
                          hint: 'Email',
                          icon: Icon(Icons.email),
                          controller: controller.emailController,
                          validator: (val){
                            if(val == null || val.isEmpty)
                              return 'email is required';
                            if(!val.isEmail)
                              return 'must be an email text';
                          },
                        ),
                        SizedBox(
                          height: 5.w,
                        ),
                        AppTextFormField(
                          hint: 'password',
                          icon: Icon(Icons.lock),
                          isPass: true,
                          controller: controller.passController,
                          validator: (val){
                            if(val == null || val.isEmpty)
                              return 'password is required';
                            if(val.length < 6)
                              return 'must be 6 characters at least';
                          },
                        ),
                        SizedBox(
                          height: 5.w,
                        ),
                        SubmitButton(
                          label: 'Login',
                          onTap: controller.login
                        )
                      ],
                    ),
                  ),
                ),
              )
          )
        ],
      ),
    );
  }
}
