
class ExamModel {

  final String course;
  final int mark;
  final bool isSuccess;

  ExamModel({
    required this.course,
    required this.mark,
    required this.isSuccess
  });

  factory ExamModel.fromJson(Map<String,dynamic> json) =>
      ExamModel(
          course: json['course']['name'],
          mark: json['mark'],
          isSuccess: json['is_success']
      );
}