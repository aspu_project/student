import 'node_model.dart';

class CourseModel {
  final int id;
  final String name;
  final int hours;
  final List<NodeModel>? parentsOr;

  CourseModel({
    required this.id,
    required this.name,
    required this.hours,
    required this.parentsOr,
  });

  factory CourseModel.fromJson(Map<String, dynamic> json) => CourseModel(
    id: json["id"],
    name: json["name"],
    hours: json["hours"],
    parentsOr: (json["parents_or"] as List?)?.map((e) => NodeModel.fromJson(e)).toList(),
  );

}