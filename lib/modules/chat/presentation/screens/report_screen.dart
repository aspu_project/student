import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/routes/get_route.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:student_chat_app/core/core_components/app_text_form_feild.dart';
import 'package:student_chat_app/core/core_components/submit_button.dart';
import 'package:student_chat_app/modules/chat/data/model/message_model.dart';
import 'package:student_chat_app/modules/chat/presentation/controller/report/report_binding.dart';

import '../controller/report/report_controller.dart';

class ReportScreen extends GetView<ReportController> {
  const ReportScreen({Key? key}) : super(key: key);

  static const _name = '/report';
  static final page = GetPage(
      name: _name,
      page: ()=> const ReportScreen(),
      binding: ReportBinding()
  );

  static void navigate(MessageModel model) =>
      Get.toNamed(_name,arguments: model);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Report'),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(2.5.w),
        child: Column(
          children: [
            ListTile(
              title: Text('Message :'),
              subtitle: Text(controller.model.answer),
            ),
            SizedBox(
              height: 5.w,
            ),
            AppTextFormField(
              hint: 'Write your report',
              maxLines: 6,
              controller: controller.reasonController,
            ),
            SizedBox(
              height: 5.w,
            ),
            SubmitButton(
              onTap: controller.submit,
            )
          ],
        ),
      ),
    );
  }
}
