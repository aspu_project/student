import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:student_chat_app/core/core_components/state_component.dart';
import 'package:student_chat_app/modules/profile/presentation/components/exam_view.dart';
import 'package:student_chat_app/modules/profile/presentation/controller/profile_binding.dart';
import 'package:student_chat_app/modules/profile/presentation/controller/profile_controller.dart';
import 'package:intl/intl.dart';
import '../../../../core/consts/color_const.dart';
import '../../../../core/core_components/bottom_nav_bar.dart';
import '../../../../core/core_components/home_drawer.dart';
import '../../data/model/user_model.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class ProfileScreen extends GetView<ProfileController> {
  const ProfileScreen({Key? key}) : super(key: key);

  static const name = '/profile';
  static final GetPage page = GetPage(
      name: name,
      page: () => const ProfileScreen(),
      binding: ProfileBinding()
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Profile'),
      ),
      drawer: const HomeDrawer(),
      bottomNavigationBar: const BottomNavBar(inx: 0,),
      body: GetBuilder<ProfileController>(
        builder: (_)=>StatusComponent<UserModel>(
            state: controller.state,
            onSuccess: (_,state)=>DefaultTextStyle(
              style: TextStyle(
                color: AppColors.lightGrey,
                fontSize: 14.spa
              ),
              child: Padding(
                padding:EdgeInsets.all(2.5.w),
                child: CustomScrollView(
                  slivers: [
                    SliverList(
                        delegate: SliverChildListDelegate(
                            [
                              SizedBox(
                                height: 60.w,
                                width: 60.w,
                                child: SfCircularChart(
                                  palette: [
                                    AppColors.blue,
                                    AppColors.darkColor,
                                  ],
                                  series: <DoughnutSeries<num, String>>[
                                    DoughnutSeries(
                                      dataSource: [controller.data.gpa/4,1-controller.data.gpa/4],
                                      xValueMapper: (value,__) => '',
                                      yValueMapper: (value,__) => value,
                                      dataLabelMapper: (value,i) => i == 0 ? controller.data.gpa.toString():'GPA',
                                      dataLabelSettings: DataLabelSettings(isVisible: true),
                                    ),
                                  ],

                                ),
                              ),
                              Container(
                                alignment: Alignment.center,
                                padding: EdgeInsets.symmetric(
                                    vertical: 2.5.w
                                ),
                                child: Text(
                                    controller.data.name,
                                    style: TextStyle(
                                        fontSize: 18.spa,
                                        fontWeight: FontWeight.bold
                                    )
                                ),
                              ),
                              const Divider(),
                              ListTile(
                                leading: const Icon(Icons.date_range),
                                title: Text('Started at : ${DateFormat("yyyy - MMM - dd").format(controller.data.startedAt)}'),
                              ),
                              ListTile(
                                leading: const Icon(Icons.school),
                                title: Text('Studying year : ${controller.data.year}'),
                              ),
                              ListTile(
                                leading: const Icon(Icons.schedule),
                                title: Text('Passed Hours : ${controller.data.hours}'),
                              ),
                              ListTile(
                                leading: const Icon(Icons.bar_chart),
                                title: Text('GPA : ${controller.data.gpa}/4'),
                              ),
                              ListTile(
                                leading: const Icon(Icons.email),
                                title: Text('Student Email : ${controller.data.email}'),
                              ),
                              const Divider(),
                              const ListTile(
                                leading: Icon(Icons.text_increase_sharp),
                                title: Text('Exams Results : '),
                              ),
                            ]
                        )
                    ),
                    SliverList(
                        delegate: SliverChildBuilderDelegate(
                            (_,i) => ExamView(controller.data.exams[i]),
                            childCount: controller.data.exams.length
                        )
                    )
                  ],
                ),
              ),
            )
        ),
      ),
    );
  }
}
