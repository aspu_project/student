import '../../../../core/base_controllers/load_data_controller.dart';
import '../../data/data_source/information_data_source.dart';
import '../../data/model/information_model.dart';


class InformationController extends LoadDataController<List<InformationModel>> {
  @override
  Future<List<InformationModel>> callApi() =>
      InformationDataSource.getInformation();
}
