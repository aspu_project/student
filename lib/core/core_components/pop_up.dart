import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:student_chat_app/core/core_components/app_loader.dart';

void showLoader([Widget? loader]) => showDialog(
    context: Get.context!,
    builder: (_) =>
        PopScope(
          canPop: false,
          child: Center(
            child: loader ?? const AppLoader(),
          ),
        )
);

void showSnackBar(String label) => ScaffoldMessenger.of(Get.context!).
showSnackBar(SnackBar(
  content: Text(label),
));