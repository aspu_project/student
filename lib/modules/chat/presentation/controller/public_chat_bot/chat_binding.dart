
import 'package:get/get.dart';
import 'package:student_chat_app/modules/chat/presentation/controller/public_chat_bot/chat_controller.dart';

class ChatBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(ChatController());
  }
}