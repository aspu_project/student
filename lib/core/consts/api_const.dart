
class ApiConst {

  static const _baseUrl = 'http://192.168.137.1:8000/api';
  static const login = '$_baseUrl/login';
  static const sendInfoMessage = '$_baseUrl/send_info_message';
  static const sendCustomMessage = '$_baseUrl/send_special_message';
  static const getInfoMessage = '$_baseUrl/old_info_messages';
  static const information = '$_baseUrl/information';
  static const courses = '$_baseUrl/courses';
  static const availableCourses = '$_baseUrl/available_courses';
  static const profile = '$_baseUrl/profile';
  static const report = '$_baseUrl/reports';

  static String getCoursesById(int id) => '$courses/$id';

}