import 'package:student_chat_app/core/network/network_helper.dart';
import 'package:student_chat_app/core/storage/storage_handler.dart';

import '../../../../core/consts/api_const.dart';

class AuthDataSource {

  static Future<void> login(String email,String password) async{
    var response = await NetworkHelper().post(
      ApiConst.login,
      body: {
        'email':email,
        'password':password
      }
    );
    await StorageHandler().setToken(response.data['data']['token']);
  }

}
