import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:student_chat_app/core/consts/color_const.dart';
import 'package:student_chat_app/modules/chat/data/model/message_model.dart';
import 'package:student_chat_app/modules/chat/presentation/components/app_animated_text.dart';

import '../screens/report_screen.dart';

class MessageComponent extends StatelessWidget {
  final MessageModel model;
  final bool animate;
  const MessageComponent({Key? key, required this.model, this.animate = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTextStyle(
      style: TextStyle(
        color: AppColors.white,
        fontSize: 13.spa
      ),
      child: Column(
        children: [
          const ListTile(
            leading: Icon(Icons.person_rounded),
            title: Text('Student'),
          ),
          Container(
            width: double.infinity,
            margin: EdgeInsets.only(right: 15.w,left: 5.w),
            padding: EdgeInsets.all(2.5.w),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5.w).copyWith(
                topLeft: Radius.zero
              ),
              color: AppColors.blue
            ),
            child: Text(model.question),
          ),
          const Directionality(
            textDirection: TextDirection.rtl,
            child: ListTile(
              leading: Icon(Icons.android_sharp),
              title: Text('Bot'),
            ),
          ),

          Row(
            children: [
              SizedBox(
                width: 15.w,
                height: 15.w,
                child: IconButton(
                  icon: Icon(Icons.report_outlined),
                  onPressed: ()=> ReportScreen.navigate(model),
                ),
              ),
              Expanded(
                child: Container(
                  width: double.infinity,
                  margin: EdgeInsets.only(right: 5.w),
                  padding: EdgeInsets.all(2.5.w),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5.w).copyWith(
                          topRight: Radius.zero
                      ),
                      color: AppColors.darkColor
                  ),
                  child: animate ? AppAnimatedText(
                    model.answer,
                    duration: Duration(milliseconds: 10),
                  ):Text(model.answer,),
                ),
              ),

            ],
          ),
        ],
      ),
    );
  }
}
