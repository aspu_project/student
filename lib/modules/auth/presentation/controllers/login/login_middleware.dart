import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:student_chat_app/core/storage/storage_handler.dart';
import 'package:student_chat_app/modules/courses/presentation/screens/all_courses_screen.dart';

class LoginMiddleware extends GetMiddleware {


  @override
  RouteSettings? redirect(String? route) {
    if(StorageHandler().hasToken){
      return const RouteSettings(name: AllCoursesScreen.name);
    }
  }
}