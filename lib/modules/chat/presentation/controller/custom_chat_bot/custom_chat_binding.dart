
import 'package:get/get.dart';
import 'custom_chat_controller.dart';

class CustomChatBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(CustomChatController());
  }
}