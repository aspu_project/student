
import 'package:student_chat_app/core/consts/api_const.dart';
import 'package:student_chat_app/core/network/network_helper.dart';
import 'package:student_chat_app/modules/chat/data/model/message_model.dart';

class ChatDataSource {
  
  static Future<List<MessageModel>> getOldInfoMessages()async{
    var response = await NetworkHelper().get(ApiConst.getInfoMessage);
    List messages = response.data['data'];
    return messages.map(
            (e) => MessageModel.formJson(e)
    ).toList();
  }

  static Future<MessageModel> sendInfoMessages(String question)async{
    var response = await NetworkHelper().post(
        ApiConst.sendInfoMessage,
        body:{
          "question":question
        }
    );
    return MessageModel.formJson(response.data['data']);
  }
  
  static Future<void> sendReport(String reason,int messageId)=>
    NetworkHelper().post(
        ApiConst.report,
        body:{
          'reason':reason,
          'message_id':messageId
        }
    );

  static Future<MessageModel> sendCustomMessages(String question)async{
    var response = await NetworkHelper().post(
        ApiConst.sendCustomMessage,
        body:{
          "question":question
        }
    );
    return MessageModel.formJson(response.data['data']);
  }
  
}