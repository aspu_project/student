import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:student_chat_app/core/core_components/state_component.dart';
import 'package:student_chat_app/modules/courses/presentation/components/course_view.dart';
import 'package:student_chat_app/modules/courses/presentation/controller/all_courses/all_courses_binding.dart';
import '../../../../core/core_components/bottom_nav_bar.dart';
import '../../../../core/core_components/home_drawer.dart';
import '../../../information/presentation/screens/info_screen.dart';
import '../controller/all_courses/all_courses_controller.dart';


class AllCoursesScreen extends GetView<AllCoursesController> {
  const AllCoursesScreen({Key? key}) : super(key: key);

  static const name = '/all_courses_screen';
  static final GetPage page = GetPage(
    name: name,
    page: () => const AllCoursesScreen(),
    binding: AllCoursesBinding()
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Courses'),
      ),
      drawer: const HomeDrawer(),
      bottomNavigationBar: BottomNavBar(inx: 1,),
      body: GetBuilder<AllCoursesController>(
        builder: (_)=> StatusComponent(
            state: controller.state,
            onSuccess: (_,state) =>
                ListView.separated(
                  padding: EdgeInsets.all(2.5.w),
                  itemCount: controller.data.length,
                  separatorBuilder: (_,i) => const Divider(),
                  itemBuilder: (_,i) =>
                    CourseView(
                        controller.data[i],
                        navigate: true,
                    ),
                )
        ),
      ),
    );
  }
}
