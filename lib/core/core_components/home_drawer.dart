import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:student_chat_app/core/storage/storage_handler.dart';
import 'package:student_chat_app/modules/auth/presentation/screens/login_screen.dart';
import '../../modules/chat/presentation/screens/chat_screen.dart';
import '../../modules/chat/presentation/screens/custom_chat_bot_screen.dart';
import '../../modules/courses/presentation/screens/available_courses_screen.dart';

class HomeDrawer extends StatelessWidget {
  const HomeDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: [
          DrawerHeader(
              child: Align(
                alignment: Alignment.center,
                child: Text(
                  'ASPU',
                  style: TextStyle(
                      fontSize: 22.spa,
                      fontWeight: FontWeight.bold
                  ),
                ),
              )
          ),
          ListTile(
            leading: Icon(Icons.school_outlined),
            title: Text('Available Courses'),
            onTap: ()=> Get.toNamed(AvailableCoursesScreen.name),
          ),
          ListTile(
            leading: Icon(Icons.chat),
            title: Text('public chat bot'),
            onTap: ()=>Get.toNamed(ChatScreen.name),
          ),
          ListTile(
            leading: Icon(Icons.chat_outlined),
            title: Text('custom chat bot'),
            onTap: ()=>Get.toNamed(CustomChatScreen.name),
          ),
          ListTile(
            leading: Icon(Icons.logout_rounded),
            title: Text('logout'),
            onTap: ()async{
              await StorageHandler().removeToken();
              Get.offAllNamed(LoginScreen.name);
            },
          ),
          const Spacer(
            flex: 5,
          ),
          const Text('Copyright © 2024 ASPU. All rights reserved.'),
          const Spacer(),
        ],
      ),
    );
  }
}
