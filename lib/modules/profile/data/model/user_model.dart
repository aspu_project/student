import 'exam_model.dart';

class UserModel {
  final int id;
  final String name;
  final String email;
  final String role;
  final int studentId;
  final int year;
  final int hours;
  final num gpa;
  final DateTime startedAt;
  final List<ExamModel> exams;

  UserModel({
    required this.id,
    required this.name,
    required this.email,
    required this.role,
    required this.studentId,
    required this.year,
    required this.hours,
    required this.gpa,
    required this.startedAt,
    required this.exams,
  });

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
    id: json["id"],
    name: json["name"],
    email: json["email"],
    role: json["role"],
    studentId: json['student']["id"],
    year: json['student']["year"],
    hours: json['student']["hours"],
    gpa: json['student']["gpa"],
    startedAt: DateTime.parse(json['student']["started_at"]),
    exams: (json['student']['exams'] as List?)?.map((e) => ExamModel.fromJson(e)).toList() ?? <ExamModel>[]
  );

}