
import '../../../../core/consts/api_const.dart';
import '../../../../core/network/network_helper.dart';
import '../../../../core/storage/storage_handler.dart';
import '../model/information_model.dart';

class InformationDataSource {

  static Future<List<InformationModel>> getInformation()async{
    var response = await NetworkHelper().get(ApiConst.information);
    List models = response.data['data'];
    return models.map((e) =>InformationModel.fromJson(e)).toList();
  }

}
