
import 'package:get/get.dart';

import 'available_courses_controller.dart';

class AvailableCoursesBinding extends Bindings {


  @override
  void dependencies() {
    Get.put(AvailableCoursesController());
  }
}