
import 'package:student_chat_app/core/consts/api_const.dart';
import 'package:student_chat_app/core/network/network_helper.dart';

import '../model/user_model.dart';

class ProfileDataSource {


  static Future<UserModel> getProfile() async {
    var response = await NetworkHelper().get(ApiConst.profile);
    return UserModel.fromJson(response.data['data']);
  }

}